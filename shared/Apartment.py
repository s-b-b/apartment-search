"""Contains Apartment class used to unify format of downloaded data."""
import re

PHONEREGEX = re.compile(r'(?:\D\D|^\D|^|\+\d\d )('
                        r'((\d{3}( |-)?){2}\d{3})|'  # Matches 123 456 789
                        r'((\d{2}( |-)?){3}\d{3})'  # Matches 12 34 56 789
                        r')(?:\D\D|\D$|$)')

STREETREGEX = re.compile(r'[A-Z][a-z]+( [a-zA-Z]*)*\d+')

PRICEREGEX = re.compile(r'(?:\D{2,}|^)'  # Start of line or >2 non-digit
                        r'(\d+(?: |\.|,)?\d+)'  # Digits with optional SP or ,
                        r'(?:\D{2,}|$)',  # End of line or >2 non-digit
                        re.M)


class Apartment(object):
    """Used to unify format of downloaded data and easier handling."""

    def __init__(self, link, title, description,
                 price, phone=None, street=None):
        super(Apartment, self).__init__()
        self._link = link
        self._title = title
        self._description = description.strip()
        self._price = price  # TODO: if price is string, it can contain 'zł'.
        self._phone = phone  # TODO: if phone is None -> extract from desc.
        self._street = street
        self._commutingTime = None

    @property
    def link(self):
        return self._link

    @property
    def title(self):
        return self._title

    @property
    def description(self):
        return self._description

    @property
    def price(self):
        if type(self._price) is str:
            pricestr = PRICEREGEX.search(self._price)[1]
            self._price = int(re.sub(r'[ \.,]', '', pricestr))
        return self._price

    @property
    def phone(self):
        print('WARNING! Using old PHONEREGEX')
        if self._phone is None:
            self._phone = PHONEREGEX.search(self._description)[1].strip()
        return self._phone

    @property
    def street(self):
        return self._street

    @property
    def commutingTime(self):
        raise NotImplementedError
        if self._commutingTime is None:
            pass
            # Calculate _commutingTime
            # Save calculated time to _commutingTime
        return self._commutingTime

    def __str__(self):
        s = ('link={}\n'
             'title={}\n'
             'description={}\n'
             'price={}\n'
             'phone={}\n'
             'street={}\n'
             'commutingTime={}\n').format(
                 self.link,
                 self.title,
                 self.description,
                 self.price,
                 self.phone,
                 self.street,
                 self.commutingTime)
        return s
