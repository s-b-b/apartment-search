from shared import Apartment
import pytest


@pytest.mark.parametrize('phone,expected', [
    (' 745 368 754 ', '745 368 754'),
    (' +54 438 934 785 ', '438 934 785'),
    ('f df  75 78 34 679 hfj hsakd', '75 78 34 679'),
    (' +64 45 34 646 ', '64 45 34 646')
])
def test_PHONEREGEX(phone, expected):
    match = Apartment.PHONEREGEX.search(phone)
    if expected is not None:
        assert match[1].strip() == expected
    else:
        assert match is None


@pytest.mark.parametrize('price,expected', [
    ('2500 zł', '2500'),
    ('3,000 zł', '3,000'),
    ('2 700', '2 700'),
    ('dhsj2 30jdkf', '2 30'),
    ('hdfj3,40fk', '3,40'),
    ('56 78 09 09 478', None)
])
def test_PRICEREGEX(price, expected):
    match = Apartment.PRICEREGEX.search(price)
    if expected is not None:
        assert match[1] == expected
    else:
        assert match is None


def test_Apartment():
    link = "I'm a link"
    title = "This is my title"
    description = ("And this is my description containing with 789 232 378"
                   "as my phone.")
    price = "I also contain 'dirty' price which is 2,500 PLN"
    apart = Apartment.Apartment(link, title, description, price)
    assert apart.link == link
    assert apart.title == title
    assert apart.description == description
    assert apart.price == 2500
    assert apart.phone == '789 232 378'
    """assert str(apart) == ('link={}\n'
                          'title={}\n'
                          'description={}\n'
                          'price={}\n'
                          'phone={}\n'
                          'street={}\n'
                          'commutingTime={}\n').format(
                              link, title, description,
                              2500, '789 232 378',  # commutingTime
                          )"""
