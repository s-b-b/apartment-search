"""Contains basic tools used by all scrapers."""

from urllib.request import urlopen, urlretrieve
from urllib.error import URLError
from bs4 import BeautifulSoup as BS


def getBS(url):
    soup = None
    try:
        content = urlopen(url)
    except URLError as err:
        print(err)
    else:
        soup = BS(content.read(), "lxml")
        content.close()
    finally:
        return soup
