if __name__ == '__main__':
    if __package__ is None:
        import sys
        from os import path
        sys.path.append(path.dirname(path.abspath('.')))
        from Apartment import Apartment
    else:
        from ..Apartment import Apartment
    from scrapingTools import getBS
else:
    from scrapers.scrapingTools import getBS


def scrape(url='https://www.gumtree.pl/s-mieszkania-i-domy-do-wynajecia/'
           '{city}/page-{page}/v1c9008l3200008p{page}?pr=,{maxPrice}',
           pages=1, city='warszawa', maxprice=''):
    for page in range(1, pages + 1):
        url2 = url.format(page=page,
                          city=city,
                          maxPrice=maxprice)
        soup = getBS(url2)
        listings = soup.findAll('div', {'class': 'view'})
        for l in listings:
            links = l.findAll('a', {'class': 'href-link'})
            for link in links:
                # It's a generator to allow "jumping" between sites without
                # the need to wait
                yield(getApartment('https://www.gumtree.pl{link}'.format(link=link['href'])))


def getApartment(link):
    soup = getBS(link)
    if soup is not None:
        title = soup.find('span', {'class': 'myAdTitle'}).string
        description = soup.find('div', {'class': 'description'}).get_text()
        price = soup.find('div', {'class': 'price'}).span.span.string
        phone = soup.find('a', {'class': 'button telephone'})['href'][4:]
        return Apartment(link, title, description, price, phone)
